namespace Membership.API.Models
{
    public class Local
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
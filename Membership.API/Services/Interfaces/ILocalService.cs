using System.Collections.Generic;
using Membership.API.Models;

namespace Membership.API.Services.Interfaces
{
    public interface ILocalService
    {
        Local GetById(int id);
        List<Local> GetAll();
    }
}
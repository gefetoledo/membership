using System.Collections.Generic;
using System.Linq;
using Membership.API.Data.Context;
using Membership.API.Models;
using Membership.API.Services.Interfaces;

namespace Membership.API.Services.Implementations
{
    public class LocalService : ILocalService
    {
        protected readonly DataContext _dataContext;

        public LocalService(DataContext dataContext)
        {
            _dataContext = dataContext;
        }
        
        public Local GetById(int id)
        {
            return _dataContext.Locals.FirstOrDefault(x => x.Id == id);
        }

        public List<Local> GetAll()
        {
            return _dataContext.Locals.ToList();
        }
    }
}
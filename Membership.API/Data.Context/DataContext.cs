using Membership.API.Models;
using Microsoft.EntityFrameworkCore;

namespace Membership.API.Data.Context
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }
        
        public DbSet<Local> Locals { get; set; }
        public DbSet<User> Users { get; set; }
    }
}
using Membership.API.Services.Implementations;
using Membership.API.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Membership.API.Middleware
{
    public static class IoC
    {
        public static IServiceCollection AddRegistration(this IServiceCollection services)
        {
            services.AddTransient<ILocalService, LocalService>();
            return services;
        }
    }
}